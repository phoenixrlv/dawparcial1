

// (1) completar los package y los import
package controlador;
import java.io.IOException;
import modelo.Modelo;
import modelo.ModeloHashset;
import vista.Vista;

public class Main {
    public static void main(String[] args) throws IOException {        
        Modelo modelo = new ModeloHashset(); // (2) Instanciar el ModeloHashset
        Vista vista = new Vista();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
