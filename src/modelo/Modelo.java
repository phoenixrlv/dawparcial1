
package modelo;

import java.util.HashSet;

public interface Modelo {

    public int getId();

    public void create(Alumno alumno);

    public HashSet read();

    
}
