
package modelo;

public class Persona {
        
// (4) Completar los atributos id y nombre de persona.
    protected String id;
    protected String nombre;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
 
}
